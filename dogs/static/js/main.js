$(document).ready(function() {
    try {
        function csrf_token() {
            return $("#csrf-token").attr("data-token");
        };

        $(".breed-select").select2({
            ajax: {
                url: "/api/breeds",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term
                    };
                },
                cache: true
            },
            minimumInputLength: 1,
            placeholder: "We love all kinds of puppies..."
        });

        $(".age-select").select2({
            placeholder: "How old is he?"
        });

        $('.ajax').magnificPopup({
            type: 'ajax'
        });

        //magnific popup
        var mfpImage = {
            tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
            titleSrc: function(item) {
                return '<a href="{0}">{1} <small></small>'
                    .format(
                        item.el.attr('data-gallery-url'),
                        item.el.attr('title')
                    );
            }
        }

        $('.popup-gallery').not(".dog-can-do .popup-gallery").magnificPopup({
            delegate: 'a',
            type: 'image',
            tLoading: 'Loading image #%curr%, please wait...',
            mainClass: 'mfp-img-mobile',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0,1]
            },
            image: mfpImage
        });

        $('.popup-image').magnificPopup({
            delegate: 'a',
            type: 'image',
            tLoading: 'Loading image #%curr%, please wait...',
            mainClass: 'mfp-img-mobile',
            image: mfpImage
        });

        var upload_targets = [];
        $('#direct_upload input[type="file"]')
            .cloudinary_fileupload({
                dropZone: '#direct_upload',
                start: function () {
                    var target = $('<div class="col-md-3 popup-gallery"><a href="#" class="thumbnail thumbnail-basic fa fa-cloud-upload fa-3x"></a></div>');
                    upload_targets.push(target);
                    target.insertAfter($("#thumbnails").children().eq(0))
                    console.log('Starting direct upload...');
                },
                progress: function () {
                    console.log("uploading");
                },
            })
            .on('cloudinarydone', function (e, data) {
                $.post(this.form.action, $(this.form).serialize()).always(function (result, status, jqxhr) {
                    var target = upload_targets.pop();
                    target.replaceWith(result);
                });
            });

        //
        // dog can do
        //
        $(".dog-can-do").on('click', 'a.thumbnail', function(event) {
            var result = confirm("Do you want to choose this picture?");
            event.preventDefault();
            if (result != true) {
                return;
            }
            var picture_id = $(this).attr("data-picture-id");
            var form = $("#dog_can_do_form");
            form.find("input[name=picture_id]")[0].value = picture_id;
            form.submit();
        });

        //
        //editor
        //
        var editable = $("#post_body");
        var source = $('#id_body');

        editable.hallo({
            plugins: {
                'halloformat': {},
                'halloheadings': {},
                'hallolists': {},
                'hallojustify': {},
                'hallolink': {},
                'halloreundo': {}
            },
            toolbar: 'halloToolbarFixed'
        });

        editable.bind('hallomodified', function(event, data) {
            update_source(data.content);
        });

        editable.bind('halloactivated', function(event, data) {
            //select content
            var range = rangy.createRange();
            range.selectNodeContents(editable[0]);
            var sel = rangy.getSelection();
            sel.removeAllRanges();
            sel.addRange(range);
        });

        function convert_to_markdown(content) {
            var html = content.split("\n").map($.trim).filter(function(line) {
                return line != "";
            }).join("\n");
            return toMarkdown(html);
        };

        function update_source(content) {
            var markdown = convert_to_markdown(content);
            if (source.get(0).value == markdown) {
                return;
            }
            source.get(0).value = markdown;
        };

        if(editable.length > 0) {
            update_source(editable.html());
        }

        //
        //Parsley
        //
        window.ParsleyConfig = {
            errorClass: 'has-error',
            successClass: 'has-success',
            classHandler: function(ParsleyField) {
                return ParsleyField.$element.parents('.form-group');
            },
            errorsContainer: function(ParsleyField) {
                return ParsleyField.$element.parents('.form-group');
            },
            errorsWrapper: '<span class="help-block">',
            errorTemplate: '<div></div>'
        };
    } catch(err) {
        Raven.captureException(err);
    }
});


String.prototype.format = function() {
    var s = this;
    var i = arguments.length;
    while (i--) {
        s = s.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i]);
    }
    return s;
};

