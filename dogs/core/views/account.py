from datetime import date
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect
from django.contrib.auth import logout as auth_logout, get_user_model, login
from dogs.core.forms import AddDogForm, RegisterForm, LoginForm, PhotoDirectForm
from dogs.core.models import Dog, Picture
import django.contrib.auth


@login_required
def profile(request):
    return render(request, 'core/account/profile.html', {})


# TODO replace by {% url 'account_logout' %} (requires POST)
@login_required
def logout(request):
    auth_logout(request)
    return redirect('/')


@login_required
def add_dog(request):
    if request.method == 'POST':
        form = AddDogForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            breed = form.cleaned_data['breed']
            born = date(year=date.today().year - form.cleaned_data['age'], month=1, day=1)
            dog_ = Dog.objects.create(name=name, breed=breed, owner=request.user, born=born)
            return redirect(dog_.get_absolute_url())
    else:
        form = AddDogForm()

    return render(request, 'core/account/dogs/add.html', {
        'form': form,
    })


@login_required
def add_dog_picture(request, dog):
    dog = Dog.objects.get(id=dog)
    picture = Picture()
    picture.gallery = dog
    picture.image = request.POST['image']
    picture.save()
    return render(request, 'core/gallery/ajax/thumbnail.html', {"picture": picture})


def authenticate(request):
    if 'password_confirmed' in request.POST:
        register_form = RegisterForm(request.POST)
        if register_form.is_valid():
            username = request.POST['username']
            password = request.POST['password']
            email = request.POST['email']
            get_user_model().objects.create_user(username, email, password)
            user = django.contrib.auth.authenticate(username=username, password=password)
            login(request, user)
            return redirect('profile')
    else:
        register_form = RegisterForm()
    login_form = LoginForm()
    return render(request, 'core/account/authenticate.html', {
        'register_form': register_form,
        'login_form': login_form,
    })
