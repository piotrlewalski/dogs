from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from dogs.core.models import Dog, Picture


class DogList(ListView):
    model = Dog
    template_name = "core/dogs/list.html"


class DogDetails(DetailView):
    model = Dog
    template_name = "core/dogs/details.html"
