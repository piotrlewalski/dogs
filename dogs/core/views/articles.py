from django.contrib.contenttypes.models import ContentType
from django.shortcuts import redirect
from django.views.generic.detail import DetailView
from django.views.generic.edit import UpdateView
from django.views.generic.list import ListView
from dogs.core.models import Article, Sport, Picture, Dog, DogAbility
from dogs.core.models import Trick
from pybb.views import TopicView


class ArticleList(ListView):
    model = Article
    template_name = "core/base_articles/articles/list.html"


class ArticleDetails(DetailView):
    model = Article
    template_name = "core/base_articles/articles/details.html"

    def get_context_data(self, **kwargs):
        topic = TopicView()
        topic.request = self.request
        topic.kwargs = dict(pk=self.object.topic.id)
        topic.topic = self.object.topic
        topic.object_list = topic.get_queryset()
        topic_data = topic.get_context_data()
        topic_data['post_list'] = topic_data['post_list'].reverse()

        data = super().get_context_data(**kwargs)
        topic_data.update(data)
        return topic_data


class TrickList(ListView):
    model = Trick
    template_name = "core/base_articles/tricks/list.html"


class TrickDetails(ArticleDetails):
    model = Trick
    template_name = "core/base_articles/tricks/details.html"


class SportList(ListView):
    model = Sport
    template_name = "core/base_articles/sports/list.html"


class SportDetails(ArticleDetails):
    model = Sport
    template_name = "core/base_articles/sports/details.html"


# TODO check dog owner
class DogCanDo(UpdateView):
    model = Trick
    fields = []
    template_name = "core/base_articles/tricks/can_do.html"

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        data['dog'] = Dog.objects.get(pk=self.kwargs['dog'])
        return data

    def post(self, request, pk, dog, slug):
        picture_id = request.POST['picture_id']
        dog_content_type = ContentType.objects.get_for_model(Dog)
        try:
            ability = DogAbility.objects.get(picture__object_id=dog, picture__content_type=dog_content_type, trick=pk)
            ability.picture_id=picture_id
        except DogAbility.DoesNotExist:
            ability = DogAbility(picture_id=picture_id, trick_id=pk)
        ability.save()
        return redirect('trick', pk=pk, slug=slug)

