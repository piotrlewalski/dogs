import json
from django.http.response import HttpResponse
from django.shortcuts import render
from dogs.core.models import Breed


def api_list(request):
    name = request.GET['q']
    items = Breed.objects.filter(name__contains=name)[:10]
    items = list(items)
    if len(items) > 0:
        results = [{"text": breed.name, "id": breed.name} for breed in items]
    else:
        results = [{"text": "Crossbreed", "id": "Crossbreed"}, {"text": "Other", "id": "Other"}]  # TODO don't hardcode this data here
    return HttpResponse(json.dumps({"results": results}), content_type="application/json")


def index(request):
    breeds = Breed.objects.all()
    return render(request, 'core/breeds/index.html', {"breeds": breeds})
