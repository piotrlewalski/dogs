from django.shortcuts import render
from dogs.core.models import Article, Sport, Trick


def home(request):
    objects = list()
    # TODO build this dynamically
    objects.append(Article.objects.get(pk=6))
    objects.append(Article.objects.get(pk=7))
    objects.append(Trick.objects.get(pk=2))
    objects.append(Sport.objects.get(pk=2))
    objects.append(Article.objects.get(pk=1))
    objects.append(Sport.objects.get(pk=1))
    objects.append(Trick.objects.get(pk=1))
    objects.append(Article.objects.get(pk=2))
    return render(request, 'home.html', {"objects": objects})


def privacy_policy(request):
    return render(request, 'about/privacy_policy.html', {})


def terms_of_service(request):
    return render(request, 'about/terms_of_service.html', {})


def support(request):
    return render(request, 'about/support.html', {})
