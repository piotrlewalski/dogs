from django.contrib import admin
from dogs.core.models import Article, DogAbility
from dogs.core.models import Trick
from dogs.core.models import Breed
from dogs.core.models import Dog
from dogs.core.models import Picture
from dogs.core.models import Sport
from dogs.core.models import RelatedArticle
from dogs.core.models import CommentsTopic

admin.site.register(Article)
admin.site.register(Sport)
admin.site.register(Trick)
admin.site.register(Breed)
admin.site.register(Dog)
admin.site.register(Picture)
admin.site.register(RelatedArticle)
admin.site.register(CommentsTopic)
admin.site.register(DogAbility)
