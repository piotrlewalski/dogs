from django.conf.urls import patterns, url
from dogs.core.views.articles import ArticleList, ArticleDetails, TrickList, TrickDetails, SportList, SportDetails, \
    DogCanDo
from dogs.core.views.dogs import DogList, DogDetails

urlpatterns = patterns(
    '',
    url(r'^$', 'dogs.core.views.home', name='home'),
    url(r'^about/privacy-policy$', 'dogs.core.views.privacy_policy', name='privacy_policy'),
    url(r'^about/privacy_policy$', 'dogs.core.views.privacy_policy', name='privacy_policy'),
    url(r'^about/terms-of-service$', 'dogs.core.views.terms_of_service', name='terms_of_service'),
    url(r'^about/support$', 'dogs.core.views.support', name='support'),

    url(r'^account/authenticate$', 'dogs.core.views.account.authenticate', name='authenticate'),
    url(r'^account/bye-bye$', 'dogs.core.views.account.logout', name='logout'),
    url(r'^account/profile', 'dogs.core.views.account.profile', name='profile'),
    url(r'^account/dogs/add$', 'dogs.core.views.account.add_dog', name='add_dog'),
    url(r'^account/dogs/(?P<dog>\d+)/add-picture$', 'dogs.core.views.account.add_dog_picture', name='add_dog_picture'),

    # required for forum
    url(r'^account/authenticate$', 'dogs.core.views.account.authenticate', name='registration_register'),
    url(r'^account/authenticate$', 'dogs.core.views.account.authenticate', name='auth_login'),

    url(r'^articles/$', ArticleList.as_view(), name='articles'),
    url(r'^articles/(?P<pk>\d+)/(?P<slug>[\w-]+)$', ArticleDetails.as_view(), name='article'),

    url(r'^tricks/$', TrickList.as_view(), name='tricks'),
    url(r'^tricks/(?P<pk>\d+)/(?P<slug>[\w-]+)$', TrickDetails.as_view(), name='trick'),
    url(r'^tricks/(?P<pk>\d+)/(?P<slug>[\w-]+)/(?P<dog>\d+)/can-do$', DogCanDo.as_view(), name='dog_can_do'),

    url(r'^sports$', SportList.as_view(), name='sports'),
    url(r'^sports/(?P<pk>\d+)/(?P<slug>[\w-]+)$', SportDetails.as_view(), name='sport'),

    url(r'^api/breeds', 'dogs.core.views.breeds.api_list'),
    url(r'^breeds$', 'dogs.core.views.breeds.index', name='breeds'),

    url(r'^dogs$', DogList.as_view(), name='dogs'),
    url(r'^dogs/(?P<pk>\d+)/(?P<slug>[\w-]+)$', DogDetails.as_view(), name='dog'),
)
