# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_auto_20150206_2119'),
    ]

    operations = [
        migrations.AlterField(
            model_name='breed',
            name='description',
            field=models.TextField(blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='breed',
            name='name',
            field=models.CharField(max_length=256, unique=True),
            preserve_default=True,
        ),
    ]
