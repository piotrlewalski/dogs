# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0001_initial'),
        ('core', '0013_auto_20150505_2019'),
    ]

    operations = [
        migrations.CreateModel(
            name='RelatedArticle',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('parent_id', models.IntegerField(db_index=True)),
                ('object_id', models.IntegerField(db_index=True)),
                ('object_type', models.ForeignKey(related_name='related_relatedarticle', to='contenttypes.ContentType')),
                ('parent_type', models.ForeignKey(related_name='child_relatedarticle', to='contenttypes.ContentType')),
            ],
            options={
                'ordering': ['-id'],
            },
            bases=(models.Model,),
        ),
    ]
