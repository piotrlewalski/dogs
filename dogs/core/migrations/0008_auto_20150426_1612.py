# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0007_auto_20150425_1713'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='article',
            name='author',
        ),
        migrations.DeleteModel(
            name='Article',
        ),
        migrations.RenameField(
            model_name='trick',
            old_name='createDate',
            new_name='created',
        ),
        migrations.RenameField(
            model_name='trick',
            old_name='modifyDate',
            new_name='modified',
        ),
        migrations.AlterField(
            model_name='trick',
            name='text',
            field=tinymce.models.HTMLField(),
            preserve_default=True,
        ),
    ]
