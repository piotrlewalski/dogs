# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0001_initial'),
        ('core', '0009_article'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='dog',
            name='gallery',
        ),
        migrations.AddField(
            model_name='gallery',
            name='content_type',
            field=models.ForeignKey(to='contenttypes.ContentType', default=13),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='gallery',
            name='object_id',
            field=models.PositiveIntegerField(default=2),
            preserve_default=False,
        ),
    ]
