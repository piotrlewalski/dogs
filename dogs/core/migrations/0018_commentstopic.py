# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0001_initial'),
        ('core', '0017_auto_20150528_1720'),
    ]

    operations = [
        migrations.CreateModel(
            name='CommentsTopic',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('object_id', models.PositiveIntegerField()),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType')),
                ('topic', models.OneToOneField(related_name='commented_object', to='pybb.Topic')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
