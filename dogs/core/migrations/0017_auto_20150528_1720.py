# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0016_auto_20150516_0714'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='article',
            name='topic',
        ),
        migrations.RemoveField(
            model_name='sport',
            name='topic',
        ),
        migrations.RemoveField(
            model_name='trick',
            name='topic',
        ),
    ]
