# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django_markdown.models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0014_relatedarticle'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='text',
            field=django_markdown.models.MarkdownField(),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='sport',
            name='text',
            field=django_markdown.models.MarkdownField(),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='trick',
            name='text',
            field=django_markdown.models.MarkdownField(),
            preserve_default=True,
        ),
    ]
