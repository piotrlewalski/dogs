# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pybb', '0001_initial'),
        ('core', '0015_auto_20150512_1840'),
    ]

    operations = [
        migrations.AddField(
            model_name='article',
            name='topic',
            field=models.ForeignKey(default=1, to='pybb.Topic'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='sport',
            name='topic',
            field=models.ForeignKey(default=1, to='pybb.Topic'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='trick',
            name='topic',
            field=models.ForeignKey(default=1, to='pybb.Topic'),
            preserve_default=False,
        ),
    ]
