# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0018_commentstopic'),
    ]

    operations = [
        migrations.CreateModel(
            name='DogAbility',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('updated', models.DateTimeField(auto_now=True)),
                ('picture', models.ForeignKey(to='core.Picture')),
                ('trick', models.ForeignKey(to='core.Trick')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='commentstopic',
            name='topic',
            field=models.OneToOneField(related_name='commented', to='pybb.Topic'),
            preserve_default=True,
        ),
    ]
