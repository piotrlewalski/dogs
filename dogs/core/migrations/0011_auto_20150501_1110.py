# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0001_initial'),
        ('core', '0010_auto_20150430_2032'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='gallery',
            name='content_type',
        ),
        migrations.RemoveField(
            model_name='picture',
            name='gallery',
        ),
        migrations.DeleteModel(
            name='Gallery',
        ),
        migrations.AddField(
            model_name='picture',
            name='content_type',
            field=models.ForeignKey(default=13, to='contenttypes.ContentType'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='picture',
            name='object_id',
            field=models.PositiveIntegerField(default=2),
            preserve_default=False,
        ),
    ]
