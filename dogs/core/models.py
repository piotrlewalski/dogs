import datetime
from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.db import models
from cloudinary.models import CloudinaryField
from django.core.urlresolvers import reverse
from django.db.models import permalink
from django.template.defaultfilters import slugify
from django_markdown.models import MarkdownField
from genericm2m.models import RelatedObjectsDescriptor, BaseGFKRelatedObject
import rules
from pybb.models import Topic


class Picture(models.Model):
    image = CloudinaryField("image")

    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    gallery = GenericForeignKey('content_type', 'object_id')

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return "%s: %s" % (self.gallery.__class__.__name__, str(self.gallery))


class RelatedArticle(BaseGFKRelatedObject):
    class Meta:
        ordering = ['-id']

    def __str__(self):
        return "%s -> %s" % (self.parent, self.object)


class BaseArticle(models.Model):
    title = models.CharField(max_length=256)
    text = MarkdownField()
    author = models.ForeignKey(User)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    pictures = GenericRelation(Picture)
    related = RelatedObjectsDescriptor(RelatedArticle)

    states = (
        (0, 'waiting'),
        (1, 'approved'),
        (2, 'refused'),
    )
    state = models.IntegerField(choices=states, default=0)

    def __str__(self):
        #return "#%s# %s" % (self.get_state_display(), self.title)
        return self.title

    def slug(self):
        return slugify(self.title)

    @property
    def picture(self):
        try:
            return self.pictures.all()[0]
        except IndexError:
            pass

    @property
    def topic(self):
        ctype = ContentType.objects.get_for_model(self.__class__)
        return CommentsTopic.objects.get(content_type__pk=ctype.id, object_id=self.id).topic

    class Meta:
        abstract = True


class Trick(BaseArticle):
    trickDifficulty = (
        (0, 'Choose'),
        (1, 'Easy'),
        (2, 'Medium'),
        (3, 'Hard'),
    )
    difficulty = models.IntegerField(choices=trickDifficulty)

    @permalink
    def get_absolute_url(self):
        return "trick", [self.id, self.slug(), ]


class Article(BaseArticle):
    @permalink
    def get_absolute_url(self):
        return "article", [self.id, self.slug(), ]


class Sport(BaseArticle):
    @permalink
    def get_absolute_url(self):
        return "sport", [self.id, self.slug(), ]


class CommentsTopic(models.Model):
    topic = models.OneToOneField(Topic, related_name="commented")
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    object = GenericForeignKey('content_type', 'object_id')

    def __str__(self):
        return str(self.object)


class Breed(models.Model):
    name = models.CharField(max_length=256, unique=True)
    parent = models.ForeignKey('self', blank=True, null=True)
    description = models.TextField(blank=True)

    def __str__(self):
        return self.name


class Dog(models.Model):
    name = models.CharField(max_length=256)
    owner = models.ForeignKey(User, related_name='dogs')
    breed = models.ForeignKey(Breed)
    born = models.DateField()
    pictures = GenericRelation(Picture)

    def __str__(self):
        return self.name

    def age(self):
        return datetime.datetime.now().year - self.born.year

    def get_absolute_url(self):
        return reverse('dog', args=[str(self.id), slugify(self.name)])

    def title(self):
        return "%s - %s" % (self.name, self.breed)

    class Meta:
        ordering = ['-id']


class DogAbility(models.Model):
    picture = models.ForeignKey(Picture)
    trick = models.ForeignKey(Trick, related_name="dog_abilities")
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-updated']


@rules.predicate
def is_dog_owner(user, dog):
    return dog.owner == user

rules.add_perm('core.change_dog', is_dog_owner)


def dog_with_empty_gallery(self):
    return True  # FIXME


def default_dog(self):
    return self.dogs.first()


User.empty_gallery = dog_with_empty_gallery
User.default_dog = default_dog
