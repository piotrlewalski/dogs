from itertools import chain
from django import forms
from django.core.exceptions import ValidationError
from dogs.core.models import Breed
from parsley.decorators import parsleyfy
from django.forms import ModelForm
from cloudinary.forms import CloudinaryJsFileField
from .models import Picture


@parsleyfy
class AddDogForm(forms.Form):
    name = forms.CharField(
        label='Name',
        required=True,
        max_length=256,
        widget=forms.TextInput(
            attrs={'placeholder': 'However you call him...'}
        )
    )
    breed = forms.CharField(
        label='Breed',
        widget=forms.Select(attrs={'class': 'breed-select'})
    )
    age = forms.ChoiceField(
        label='Age',
        choices=chain([('', '')], zip(range(20), range(20))),
        widget=forms.Select(attrs={'class': 'age-select'})
    )

    def clean_breed(self):
        breed = self.cleaned_data['breed']
        try:
            breed = Breed.objects.get(name=breed)
        except Breed.DoesNotExist:
            raise ValidationError("This breed does not exist")
        return breed

    def clean_age(self):
        try:
            return int(self.cleaned_data['age'])
        except ValueError:
            raise ValidationError("Invalid value for age")


@parsleyfy
class RegisterForm(forms.Form):
    username = forms.CharField(
        label='',
        required=True,
        max_length=256,
        widget=forms.TextInput(
            attrs={'placeholder': 'Your username'}
        )
    )
    email = forms.EmailField(
        label='',
        widget=forms.EmailInput(
            attrs={'placeholder': 'Email'}
        )
    )
    password = forms.CharField(
        label='',
        widget=forms.PasswordInput(
            attrs={'placeholder': 'Password'}
        )
    )
    password_confirmed = forms.CharField(
        label='',
        widget=forms.PasswordInput(
            attrs={'placeholder': 'Confirm password'}
        )
    )


@parsleyfy
class LoginForm(forms.Form):
    email = forms.EmailField(
        label='',
        widget=forms.EmailInput(
            attrs={'placeholder': 'Email'}
        )
    )
    password = forms.CharField(
        label='',
        widget=forms.PasswordInput(
            attrs={'placeholder': 'Password'}
        )
    )


class PhotoDirectForm(ModelForm):
    image = CloudinaryJsFileField()

    class Meta:
        model = Picture