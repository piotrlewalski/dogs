from dogs.utils.config import config
from django.conf import settings


def processor(_):
    return dict(
        UA=config.ANALYTICS_UA,
        settings=settings,
        #SUBDOMAINS=settings.SUBDOMAINS,
        #DOMAIN_NAME=settings.DOMAIN_NAME,
    )
