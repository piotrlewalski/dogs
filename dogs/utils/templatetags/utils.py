from django import template

register = template.Library()


@register.inclusion_tag('utils/templatetags/username.html')
def username(user):
    return {
        "user": user
    }
