from dogs.utils.config import config
import os

import logging
import django.core.handlers.wsgi
from django.conf import settings
import tornado.ioloop
import tornado.web
import tornado.wsgi
import tornado.httpserver
import django.utils.importlib
import django.contrib.auth


logger = logging.getLogger(__name__)


class NoCacheStaticFileHandler(tornado.web.StaticFileHandler):
    def get_content_type(self):
        if self.absolute_path.endswith("woff2"):
            return "application/x-font-woff"
        return super().get_content_type()

    def set_extra_headers(self, path):
        self.set_header('Cache-Control', 'no-store, no-cache, must-revalidate, max-age=0')


def main():
    wsgi_app = tornado.wsgi.WSGIContainer(django.core.handlers.wsgi.WSGIHandler())

    app = tornado.web.Application([
        (r"/static/(.*)", NoCacheStaticFileHandler, {"path": os.path.join(settings.PROJECT_DIR, 'static_generated')}),
        (r"/robots.txt()$",
         tornado.web.StaticFileHandler,
         {"path": os.path.join(settings.PROJECT_DIR, 'static_generated', "robots.txt")}),
        ('.*', tornado.web.FallbackHandler, dict(fallback=wsgi_app)), ],
        debug=config.debug,
    )
    server = tornado.httpserver.HTTPServer(app)
    logger.info("starting at: http://%s:%s", config.address, config.port)
    server.listen(config.port, config.address)

    logger.info("listening at: http://%s:%s", config.address, config.port)
    tornado.ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    main()
