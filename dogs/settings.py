"""
Django settings for dogs project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import dj_database_url
import os
import dogs
from django.conf import global_settings
from dogs.utils.config import config
import cloudinary
from django.contrib.messages import constants as messages

PROJECT_DIR = os.path.dirname(dogs.__file__)
BASE_DIR = os.path.dirname(PROJECT_DIR)


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/ # TODO

SECRET_KEY = config.SECRET_KEY

DEBUG = config.DEBUG.lower() == 'true'

assert DEBUG or len(SECRET_KEY) > 10

TEMPLATE_DEBUG = DEBUG
TEMPLATE_DIRS = (os.path.join(PROJECT_DIR, 'templates'), )

TEMPLATE_LOADERS = global_settings.TEMPLATE_LOADERS + (
    'apptemplates.Loader',
)


ALLOWED_HOSTS = ['.trickydogs.net']


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'pybb',
    'genericm2m',
    'crispy_forms',
    'parsley',
    'cloudinary',
    'rules',
    'django_markdown',
    'debug_toolbar',
    'djrill',
    'dogs.core',
    'dogs.utils',
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'allauth.socialaccount.providers.facebook',
    'raven.contrib.django.raven_compat',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'pybb.middleware.PybbMiddleware',
)

ROOT_URLCONF = 'dogs.urls'

WSGI_APPLICATION = 'dogs.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

if config.database_url:
    DATABASES = {
        'default': dj_database_url.config(),
    }
else:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        }
    }


# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'

STATIC_ROOT = os.path.join(PROJECT_DIR, "static_generated")

STATICFILES_DIRS = (
    os.path.join(PROJECT_DIR, "static"),
)

TEMPLATE_CONTEXT_PROCESSORS = global_settings.TEMPLATE_CONTEXT_PROCESSORS + (
    'pybb.context_processors.processor',
    'django.core.context_processors.request',
    'pybb.context_processors.processor',
    'dogs.utils.context_processors.processor',
    #'pinax_theme_bootstrap.context_processors.theme',
    'allauth.account.context_processors.account',
    'allauth.socialaccount.context_processors.socialaccount',
)

AUTHENTICATION_BACKENDS = global_settings.AUTHENTICATION_BACKENDS + (
    'rules.permissions.ObjectPermissionBackend',
    'allauth.account.auth_backends.AuthenticationBackend',
)

LOGIN_REDIRECT_URL = '/'  # TODO
LOGIN_URL = '/account/authenticate/'

CRISPY_TEMPLATE_PACK = 'bootstrap3'

SITE_ID = 1

cloudinary.config( 
    cloud_name=config.CLOUDINARY_CLOUD_NAME,
    api_key=config.CLOUDINARY_API_KEY,
    api_secret=config.CLOUDINARY_API_SECRET,
)


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            }
    },
    'loggers': {
        'django.db.backends': {
            'handlers': ['console'],
            'level': 'INFO',
            },
        'tornado.access': {
            'handlers': ['console'],
            'level': 'ERROR',
            },
        }
}

TINYMCE_JS_URL = STATIC_URL + 'vendor/tinymce/js/tinymce/tinymce.min.js'

TINYMCE_DEFAULT_CONFIG = {
    'plugins': [ "advlist autolink lists link anchor insertdatetime contextmenu paste"],
    'theme': 'modern',
    'menubar': False,
    'statusbar': False,
    'toolbar': [
        "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | "
        "bullist numlist outdent indent | link"
    ]
}

PYBB_MARKUP = 'markdown'
PYBB_TEMPLATE = 'forum.html'
PYBB_DISABLE_SUBSCRIPTIONS = True  # TODO
PYBB_DISABLE_NOTIFICATIONS = True  # TODO


ACCOUNT_AUTHENTICATION_METHOD = "username_email"
ACCOUNT_EMAIL_REQUIRED = True
SOCIALACCOUNT_QUERY_EMAIL = True
SOCIALACCOUNT_EMAIL_REQUIRED = False

ACCOUNT_USER_DISPLAY = lambda user: user.get_full_name() or user.username

SOCIALACCOUNT_PROVIDERS = {
    'facebook':
        {
            'SCOPE': ['email', 'public_profile'],
            'AUTH_PARAMS': {'auth_type': 'reauthenticate'},
            'METHOD': 'js_sdk',
            'LOCALE_FUNC': lambda request: 'en_US',  # TODO
            'VERIFIED_EMAIL': True,  # TODO double check
            'VERSION': 'v2.3'
        }
}

MANDRILL_API_KEY = config.MANDRILL_APIKEY

EMAIL_BACKEND = "djrill.mail.backends.djrill.DjrillBackend"
DEFAULT_FROM_EMAIL = "peter@trickydogs.net"  # TODO setup

MESSAGE_TAGS = {
    messages.ERROR: 'danger'
}

RAVEN_CONFIG = {
    'dsn': config.RAVEN_DSN,
}

COMMENTS_FORUM_CATEGORY = 1

SESSION_COOKIE_AGE = 3600