from django.conf.urls import patterns, include, url
from django.contrib import admin


urlpatterns = patterns(
    '',
    url(r'^', include('dogs.core.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^markdown/', include('django_markdown.urls')),
    url(r'^forum/', include('pybb.urls', namespace='pybb')),
    url(r'^accounts/', include('allauth.urls')),
)
