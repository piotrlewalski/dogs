import os
import dogs.utils.patch
import logging


def init():
    dogs.utils.patch.patch()
    if os.path.exists(".env_local"):
        env_input = ".env_local"
    else:
        print(".env_local does not exists, using default .env file")
        env_input = ".env"

    with open(env_input) as env:
        for line in env.read().splitlines():
            if not line.strip():  # skip empty lines
                continue
            var = line.split('=')
            print(var)
            os.environ.setdefault(*var)

    logging_level = getattr(logging, os.environ.get('LOGGING_LEVEL', 'INFO'))
    logging.basicConfig(level=logging_level)
