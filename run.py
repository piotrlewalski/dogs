import sys
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "dogs.settings")

import init
init.init()

import django
django.setup()

import dogs.server
import django.core.management



link = sys.platform != 'win32'
print("Static files")
django.core.management.call_command('collectstatic', link=link, interactive=False)
print("Migrations")
django.core.management.call_command('migrate', interactive=False)
print("Loading data")
django.core.management.call_command(
    'loaddata',
    os.path.join('dogs', 'core', 'fixtures', 'initial_data.json'),
    interactive=False)
print("Removing old sessions")
django.core.management.call_command('clearsessions', interactive=False)
print("Starting")
dogs.server.main()

